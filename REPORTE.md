## **REPORTE** 



* [Guest](#guest)
* [Reporter](#reporter) 
* [Developer](#developer)
* [Mantainer](#mantainer) 



# Guest 

## Permite
* Crear issues 
* Clonar repositorio localmente 
 


### NO permite 
* Borrar issues 
* Crear ramas
* Uso de interfaz para realizar commit  
* Agregar miembros 


# Reporter 



### Permite
* Crear issues 
* Clonar repositorio  
 


### NO permite 
* Borrar issues 
* Crear ramas
* Uso de interfaz para realizar commit  
* Push 
* Pull 
* Agregar miembros 
* Crear nuevos archivos

# Developer 

### Permite
* Hacer push a las ramas  
* Hacer pull a las ramas
* Hacer merge con previa aprobacion 
* Crear ramas 
* Uso de interfaz para realizar commit 
* Editar issues 

### NO permite 
* Borrar issues 
* Push al master 
* Agregar miembros 
* Crear nuevos archivos
* Eliminar proyecto 


# Mantainer  

### Permite
* Hacer push 
* Hacer pull 
* Hacer merge  
* Crear ramas 
* Uso de interfaz para realizar commit 
* Editar issues 

### NO permite 
* Borrar issues 
* Agregar miembros 
* Crear nuevos archivos
* Eliminar proyecto 

## Localmente  

### Permite
* Crea ramas
* Uso de comando git add . 
* Uso de comando git commit -m "comentario"


